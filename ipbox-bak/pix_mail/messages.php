<?php
	$lang = array(
        // English is the default language, you can change the langugae from the config.php file
        'PL'=> array(
            'check_email'=>'Proszę o podanie poprawnego adresu E-mail!',
            'captcha'=>'Please check the Captcha!',
            'php_error'=>'Could not send mail! Please check your PHP mail configuration.',
            'success'=>'Dziękujemy, wiadomość została wysłana!',
            'subscription'=>'Thank you for your Subscription.',
            'email_exists'=>'Email Already Exists',
        )
    );
?>
