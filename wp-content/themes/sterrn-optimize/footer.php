<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>

    <footer class="footer">
      <div class="grid-x">
        <div class="cell medium-6">
          <?php the_field('footer_column_1', get_option('page_on_front')); ?>
        </div>
        <div class="cell medium-4">
          <?php the_field('footer_column_2', get_option('page_on_front')); ?>
          <div class="hide-for-medium">
            <?php the_field('footer_column_5', get_option('page_on_front')); ?>
            <?php the_field('footer_column_6', get_option('page_on_front')); ?>
          </div>
        </div>
        <div class="cell medium-2">
          <?php the_field('footer_column_3', get_option('page_on_front')); ?>
        </div>
      </div>
      <div class="grid-x">
        <div class="cell medium-6 copyrights">
          <?php the_field('footer_column_4', get_option('page_on_front')); ?>
        </div>
        <div class="cell medium-4 show-for-medium">
          <?php the_field('footer_column_5', get_option('page_on_front')); ?>
        </div>
        <div class="cell medium-2 show-for-medium">
          <?php the_field('footer_column_6', get_option('page_on_front')); ?>
        </div>
      </div>
    </footer>

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
