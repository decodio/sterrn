<?php
/**
* The template for displaying all single posts and attachments
*/

get_header(); ?>

<main class="main team-single">
    <div class="wrapper">
        <div class="content">
            <div class="page-header">
                <p class="page-single-breadcrumbs">
                    <span><a href="/zespol/">Zespół</a></span>
                    <span class="has-parent"><?php the_title(); ?></span>
                </p>
            </div>
            <div class="team-single-content">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                        <div class="grid-x grid-margin-x">
                            <div class="cell medium-5">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                            <div class="cell medium-7">
                                <header class="article-header">
                                    <h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
                                    <?php if(get_field('stanowisko')): ?>
                                    <p><?php the_field('stanowisko'); ?></p>
                                    <?php endif; ?>
                                </header> <!-- end article header -->
                                <section class="entry-content" itemprop="text">
                                    <?php the_content(); ?>
                                </section> <!-- end article section -->
                            </div>
                        </div>
                    </article> <!-- end article -->
                <?php endwhile; else : ?>
                    <?php get_template_part( 'parts/content', 'missing' ); ?>
                <?php endif; ?>
            </div>
            <div class="team-listing">
                <h2>Pozostałe osoby:</h2>
                <div class="grid-x grid-margin-x">
                    <?php
                    // the query
                    $args = array(
                        'post_type' => 'zespol',
                        'posts_per_page' => -1,
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                        'post__not_in' => array(get_the_ID())
                    );
                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="cell medium-4">
                                <div class="team-listing-item">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="team-listing-image">
                                            <?php the_post_thumbnail( 'full' ); ?>
                                        </div> 
                                        <h3><?php the_title(); ?></h3>
                                        <?php if(get_field('stanowisko')): ?>
                                        <p><?php the_field('stanowisko'); ?></p>
                                        <?php endif; ?>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
