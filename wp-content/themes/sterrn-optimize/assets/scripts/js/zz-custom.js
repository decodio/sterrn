(function( root, $, undefined ) {
	"use strict";

	$(function () {
		// DOM ready, take it away

		$('.sidebar-toggle').click(function(){
			if($('body').hasClass('nav-hidden')) {
				$('body').removeClass('nav-hidden').addClass('nav-active');
			} else {
				$('body').addClass('nav-hidden').removeClass('nav-active');
			}
		});

		$('.navigation .overflow').click(function(){
			$('body').addClass('nav-hidden').removeClass('nav-active');
		});

		function adjustNav() {
			if($('body').hasClass('home')) {
				var vH = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
				var bottomTextHeight = $('.home .content').outerHeight();
				$('.sidebar-nav').css('max-height', vH - bottomTextHeight);
			}
		}
		adjustNav();
		setInterval(adjustNav, 500);

		function adjustVH() {
			var windowHeight = window.innerHeight;
			$('.vh').css("height", windowHeight + 'px');
			$('.vh-min').css("min-height", windowHeight + 'px');
			$('.sidebar-nav').css('height', windowHeight + 'px');
		}
		adjustVH();
		window.onresize = adjustVH;

		$('.home .specializations-column ul').matchHeight();

		/* IP Box */
		if($('.ipbox').length > 0) {
			new universalParallax().init({
				speed: 6.0
			});

			var swiper = new Swiper(".testimonials .swiper-container", {
				pagination: {
					el: ".testimonials .swiper-pagination",
					clickable: true
				},
			});

			/* Forms */
			$('.form-field input').on('focus', function() {
				$(this).parents('.form-field').addClass('form-field--focused');
			});

			$('.form-field input').on('blur', function() {
				var val = $(this).val();
				$(this).parents('.form-field').removeClass('form-field--focused');
				if(val.length > 0) {
					$(this).parents('.form-field').addClass('form-field--filled');
				} else {
					$(this).parents('.form-field').removeClass('form-field--filled');
				}
			});

			$('.hero__arrow').click(function(){
				$('html, body').animate({scrollTop : $('.about').offset().top },1000);
			});

			$('.cookie-accept').click(function(e){
				e.preventDefault();
				$('.cookie-bar').addClass('cookie-bar--accepted');
				Cookies.set('ip-box-cookie-accept', 'true');
			});
		}

		// GA Events
		function formDisplay() {
			console.log({
				'event' : 'viewForm',
				'formName' : 'Kontakt Stopka'
			});
			window.dataLayer = window.dataLayer || [];
			dataLayer.push({
				'event' : 'viewForm',
				'formName' : 'Kontakt Stopka'
			});
			formViewed = true;
			return;
		}

		var formViewed = false;

		function isScrolledIntoView(elem)
		{
			var docViewTop = $(window).scrollTop();
			var docViewBottom = docViewTop + $(window).height();
			var elemTop = $(elem).offset().top;
			var elemBottom = elemTop + $(elem).height();
			return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		}

		$(window).scroll(function() {
			if($('#contact').length < 1) return;
			if(isScrolledIntoView($('#contact')) && !formViewed){
				formDisplay();
			}
		});

		function popupFormDisplay() {
			console.log({
				'event' : 'viewForm',
				'formName' : 'Kontakt Popup'
			});
			window.dataLayer = window.dataLayer || [];
			dataLayer.push({
				'event' : 'viewForm',
				'formName' : 'Kontakt Popup'
			});
			return;
		}

		$('[data-open="contact-modal"]').on('click', function(){
			popupFormDisplay();
		});

		$('.wpcf7-form input').click(function(){

			var field, form;

			if($(this).attr('type') == 'submit') return;

			switch ($(this).attr('name')) {
				case 'your-name':
				field = 'Imię i nazwisko'
				break;
				case 'your-email':
				field = 'Imię i nazwisko'
				break;
				case 'your-phone':
				field = 'Numer telefonu'
				break;
				case 'acceptance[]':
				field = 'Zgoda RODO'
				break;
				default:
				field = $(this).attr('name')
				break;
			}

			if($(this).parents('#contact').length > 0) {
				form = 'Kontakt Stopka';
			} else {
				form = 'Kontakt Popup';
			}

			console.log({
				'event' : 'fieldClick',
				'fieldName' : field,
				'formName' : form
			});

			window.dataLayer = window.dataLayer || [];
			dataLayer.push({
				'event' : 'fieldClick',
				'fieldName' : field,
				'formName' : form
			});
		});

		$('.wpcf7').on('wpcf7invalid', function( event ) {
			setTimeout(function(){
				var form;
				if($(this).parents('#contact').length > 0) {
					form = 'Kontakt Stopka';
				} else {
					form = 'Kontakt Popup';
				}
				$(event.target).find('input').each(function(){
					var field;
					if($(this).parents('.wpcf7-form-control').hasClass('wpcf7-not-valid') || $(this).hasClass('wpcf7-not-valid')){
						switch ($(this).attr('name')) {
							case 'your-name':
							field = 'Imię i nazwisko'
							break;
							case 'your-email':
							field = 'Imię i nazwisko'
							break;
							case 'your-phone':
							field = 'Numer telefonu'
							break;
							case 'acceptance[]':
							field = 'Zgoda RODO'
							break;
							default:
							field = $(this).attr('name')
							break;
						}
						window.dataLayer = window.dataLayer || [];
						dataLayer.push({
							'event' : 'fieldError',
							'fieldName' : field,
							'formName' : form
						});
						console.log({
							'event' : 'fieldError',
							'fieldName' : field,
							'formName' : form
						});
					}
				});
			},500)
		});

		$('.wpcf7').on('wpcf7mailsent', function( event ) {
			var form;
			if($(this).parents('#contact').length > 0) {
				form = 'Kontakt Stopka';
			} else {
				form = 'Kontakt Popup';
			}
			window.dataLayer = window.dataLayer || [];
			dataLayer.push({
				'event' : 'formSubmitted',
				'formName' : form
			});
			console.log({
				'event' : 'formSubmitted',
				'formName' : form
			});
		});

	});

} ( this, jQuery ));
