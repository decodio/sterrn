<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>

<main class="main page">
    <div class="wrapper">
        <div class="content">
            <div class="page-header">
                <h1>404</h1>
            </div>
            <div class="page-content">
                <h2>Podana strona nie istnieje</h2>
                <p><a href="/">Wróć na stronę główną</a></p>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
