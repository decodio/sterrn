<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
*/

get_header(); ?>

<main class="main home">
  <section class="hero vh">
    <div class="logo">
      <a href="<?php echo home_url(); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sterrn_logo-01.svg" alt="Sterrn Logo" width="198" height="28">
      </a>
    </div>
    <div class="wrapper">
      <div class="content">
        <?php if(get_field('tekst_hero')): ?>
          <h1><?php the_field('tekst_hero'); ?></h1>
        <?php endif; ?>
      </div>
      <div class="decor" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/sterrn_home.jpg)"></div>
    </div>
  </section>
  <section class="specializations">
    <div class="specializations-column">
      <h2>Podatki</h2>
      <?php if(get_field('podatki_specjalizacje')): ?>
        <ul>
          <?php $links = get_field('podatki_specjalizacje'); ?>
          <?php foreach($links as $l): ?>
            <?php $link = $l['link']; ?>
            <li><a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a></li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      <a href="#">Poznaj wszystkie specjalizacje</a>
    </div>
    <div class="specializations-column">
      <h2>Prawo</h2>
      <?php if(get_field('prawo_specjalizacje')): ?>
        <ul>
          <?php $links = get_field('prawo_specjalizacje'); ?>
          <?php foreach($links as $l): ?>
            <?php $link = $l['link']; ?>
            <li><a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a></li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      <a href="#">Poznaj wszystkie specjalizacje</a>
    </div>
  </section>
  <section class="blog">
    <h2>Najnowsze publikacje</h2>
    <div class="blog-row">
      <?php
      $sticky = get_field('blog_wpisy');
      if($sticky):
        ?>
        <?php
        $args = array(
          'posts_per_page' => 2,
          'post__in' => $sticky
        );

        $the_query = new WP_Query( $args ); ?>

        <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="blog-column">
              <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

                <header class="article-header">
                  <?php get_template_part( 'parts/content', 'byline' ); ?>
                </header> <!-- end article header -->
                <section class="entry-content" itemprop="text">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <?php the_excerpt(); ?>
                </section> <!-- end article section -->
              </article> <!-- end article -->
            </div>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      <?php endif; ?>
      <?php if(count($sticky) < 2): ?>
        <?php
        $args = array(
          'posts_per_page' => 2 - count($sticky)
        );

        $the_query = new WP_Query( $args ); ?>

        <?php if ( $the_query->have_posts() ) : ?>
          <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="blog-column">
              <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

                <header class="article-header">
                  <?php get_template_part( 'parts/content', 'byline' ); ?>
                </header> <!-- end article header -->
                <section class="entry-content" itemprop="text">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <?php the_excerpt(); ?>
                </section> <!-- end article section -->
              </article> <!-- end article -->
            </div>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </section>
</main>

<?php get_footer(); ?>
