<?php
/*
Template Name: IP Box
*/

get_header('ipbox'); ?>

<main class="ipbox">
  <article class="wrapper">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <section class="hero">
        <div class="hero__content">
          <div class="grid-container">
            <span class="hero__logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-white.svg" alt="Sterrn"></span>
            <div class="hero__wrapper">
              <h1 class="header-underlined white"><?php the_field('hero_header_underline'); ?></h1>
              <p class="hero__header-big"><?php the_field('hedro_header_big'); ?></p>
              <p class="hero__text"><?php the_field('hero_header_text'); ?></p>
              <a href="#contact" class="button hollow white" data-open="contact-modal"><span><?php the_field('hero_header_button_text'); ?><span></a>
            </div>
            <span class="hero__arrow"></span>
          </div>
        </div>
        <div class="hero__bg">
          <div class="hero__parallax">
            <style>
              @media (max-width: 41.875rem) {
                #parallax-hero {
                  background-image: url(<?php the_field('hero_header_background_mobile'); ?>) !important;
                  background-position: right center;
                }
              }
            </style>
            <div class="parallax" id="parallax-hero" style="background-image: url(<?php the_field('hero_header_background'); ?>"></div>
          </div>
        </div>
      </section>
      <section class="about">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell large-4 show-for-large">
              <div class="about__image">
                <?php echo wp_get_attachment_image( get_field('about_image'),'full' )?>
              </div>
            </div>
            <div class="cell large-8">
              <div class="about__content">
                <div class="about__text">
                  <?php the_field('about_content'); ?>
                </div>
                <div class="about__cta">
                  <a href="#contact" class="button hollow"data-open="contact-modal"><span><?php the_field('about_button_text'); ?><span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="savings">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell large-5">
              <div class="savings__text">
                <h2><?php the_field('savings_header'); ?></h2>
                <div class="savings__cta show-for-large">
                  <a href="#contact" class="button hollow white"data-open="contact-modal"><span><?php the_field('savings_button_text'); ?><span></a>
                </div>
              </div>
            </div>
            <div class="cell large-7">
              <div class="savings__table">
                <table cellpadding="0" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Twój roczny dochód (zł)</th>
                      <th>Podatek przed ulgą (zł)</th>
                      <th>Podatek z ulgą IP BOX (zł)</th>
                      <th>Zostaje<br>w kieszeni</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $rows = get_field('savings_table'); ?>
                    <?php if($rows): ?>
                      <?php foreach($rows as $r): ?>
                        <tr>
                          <td><?php echo $r['roczny_dochod']; ?></td>
                          <td><?php echo $r['podatek_przed_ulga']; ?></td>
                          <td><?php echo $r['podatek_z_ulga']; ?></td>
                          <td><?php echo $r['zysk']; ?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
              <div class="savings__cta hide-for-large">
                <a href="#contact" class="button hollow white" data-open="contact-modal"><span><?php the_field('savings_button_text'); ?><span></a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="savings__image">
        <div class="parallax" style="background-image: url(<?php the_field('savings_image'); ?>)"></div>
      </div>
      <section class="process">
        <div class="grid-container">
          <h2 class="header-underlined green"><?php the_field('process_header'); ?></h2>
          <?php $steps = get_field('process_steps'); ?>
          <?php if($steps): ?>
          <div class="process__steps">
            <?php $i = 1; ?>
            <?php foreach($steps as $s): ?>
            <div class="process__step">
              <div class="process__step-number">
                <span><?php echo $i; ?>.</span>
              </div>
              <div class="process__step-content">
                <h3><?php echo $s['header']; ?></h3>
                <?php echo $s['content']; ?>
              </div>
            </div>
            <?php $i++; ?>
            <?php endforeach; ?>
          </div>
          <?php endif; ?>
        </div>
      </section>
      <section class="testimonials">
        <div class="grid-container">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <?php $testimonials = get_field('testimonials'); ?>
              <?php if($testimonials): ?>
                <?php foreach($testimonials as $t): ?>
                  <div class="swiper-slide">
                    <div class="testimonials__item">
                        <?php echo wp_get_attachment_image( $t['image'], 'full' )?>
                        <h3><?php echo $t['name']; ?></h3>
                        <p><?php echo $t['text']; ?></p>
                    </div>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
        <div class="text-center">
          <a href="#contact" class="button hollow white" data-open="contact-modal"><span><?php the_field('testimonals_button_text'); ?><span></a>
        </div>
      </section>
      <section class="faq">
        <div class="grid-container">
          <h2><?php the_field('faq_header'); ?></h2>
          <?php $faq = get_field('faq'); ?>
          <?php if($faq): ?>
          <ul class="accordion" data-accordion data-allow-all-closed="true">
            <?php $i = 1; ?>
            <?php foreach($faq as $f): ?>
              <li class="accordion-item" data-accordion-item>
                <!-- Accordion tab title -->
                <a href="#" class="accordion-title"><span><?php echo $i; ?>.</span> <?php echo $f['header']; ?></a>
                <div class="accordion-content" data-tab-content>
                  <?php echo $f['content']; ?>
                </div>
              </li>
              <?php $i++; ?>
            <?php endforeach; ?>
          </ul>
          <?php endif; ?>
        </div>
      </section>
      <div class="contact__image hide-for-medium">
        <div class="parallax" style="background-image: url(<?php the_field('contact_form_background'); ?>)"></div>
      </div>
      <section id="contact" class="contact">
        <div class="grid-container">
          <div class="grid-x align-center">
            <div class="cell medium-10 large-8">
              <div class="contact__box">
                <?php the_field('contact_form_content'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="parallax" style="background-image: url(<?php the_field('contact_form_background'); ?>"></div>
      </section>
    <?php endwhile; else : ?>
      <?php get_template_part( 'parts/content', 'missing' ); ?>
    <?php endif; ?>
  </article>
</main>
<footer class="footer footer--ip">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell medium-6">
        <?php the_field('footer_column_1'); ?>
      </div>
      <div class="cell medium-4">
        <?php the_field('footer_column_2'); ?>
        <div class="hide-for-medium">
          <?php the_field('footer_column_5'); ?>
          <?php the_field('footer_column_6'); ?>
        </div>
      </div>
      <div class="cell medium-2">
        <?php the_field('footer_column_3'); ?>
      </div>
    </div>
    <div class="grid-x">
      <div class="cell medium-6 copyrights">
        <?php the_field('footer_column_4'); ?>
      </div>
      <div class="cell medium-4 show-for-medium">
        <?php the_field('footer_column_5'); ?>
      </div>
      <div class="cell medium-2 show-for-medium">
        <?php the_field('footer_column_6'); ?>
      </div>
    </div>
  </div>
</footer>
<div class="reveal medium" id="contact-modal" data-reveal  data-animation-in="fade-in" data-animation-out="fade-out">
  <div class="popup-contact">
    <div class="contact__box">
      <?php the_field('popup_contact_form_content'); ?>
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
</div>

<div class="cookie-bar <?php if($_COOKIE["ip-box-cookie-accept"] != 'true') echo 'active'; ?>">
  <div class="grid-container">
    <div class="cookie-bar-content">
      <p>Odwiedzając tę stronę, wyrażasz zgodę na używanie plików „cookies” zgodnie z ustawieniami Twojej przeglądarki oraz <a href="/polityka-prywatnosci">Polityką Prywatności</a>.</p>
      <button class="button cookie-accept">OK</button>
    </div>
  </div>
</div>

<?php get_footer('ipbox'); ?>
