(function( root, $, undefined ) {
	"use strict";

	$(function () {
		// DOM ready, take it away

        $('.sidebar-toggle').click(function(){
            if($('body').hasClass('nav-hidden')) {
                $('body').removeClass('nav-hidden').addClass('nav-active');
            } else {
                $('body').addClass('nav-hidden').removeClass('nav-active');
            }
        });

        $('.navigation .overflow').click(function(){
            $('body').addClass('nav-hidden').removeClass('nav-active');
        });

        function adjustNav() {
            if($('body').hasClass('home')) {
                var vH = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
                var bottomTextHeight = $('.home .content').outerHeight();
                $('.sidebar-nav').css('max-height', vH - bottomTextHeight);
            }
        }
        adjustNav();
        setInterval(adjustNav, 500);

        function adjustVH() {
            var windowHeight = window.innerHeight;
            $('.vh').css("height", windowHeight + 'px');
            $('.vh-min').css("min-height", windowHeight + 'px');
            $('.sidebar-nav').css('height', windowHeight + 'px');
        }
        adjustVH();
        window.onresize = adjustVH;

	});

} ( this, jQuery ));
