<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Adding scripts file in the footer
    wp_enqueue_script( 'anime', get_template_directory_uri() . '/assets/scripts/anime.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );

    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );


    // Register main stylesheet
    if(is_front_page()) {
      // wp_enqueue_style( 'site-home', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );
      wp_dequeue_style( 'wp-block-library' ); // WordPress core
      wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core
      wp_dequeue_style( 'wc-block-style' ); // WooCommerce
      wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
      wp_deregister_script( 'wp-embed' );
    } else {
      wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );
    }


    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
