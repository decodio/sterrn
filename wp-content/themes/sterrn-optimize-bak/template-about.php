<?php
/*
Template Name: O nas
*/

get_header(); ?>

<main class="main about">
    <div class="wrapper">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="content">
                <div class="page-header">
                    <h1><?php the_title(); ?></h1>
                </div>
                <?php if(get_field('tekst_naglowek')): ?>
                <div class="about-content">
                    <div class="about-content-top">
                        <p><?php the_field('tekst_naglowek'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="content-bottom">
                <div>
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; else : ?>
            <?php get_template_part( 'parts/content', 'missing' ); ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
