<?php
/*
Template Name: Kontakt
*/

get_header(); ?>

<main class="main contact">
    <div class="wrapper">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="content">
                <div class="page-header">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="contact-content">
                    <?php the_content(); ?>
                    <?php if(get_field('tresc_dane_kontaktowe')): ?>
                    <div class="contact-details">
                        <?php the_field('tresc_dane_kontaktowe'); ?>
                    </div>
                    <?php endif; ?>
                    <?php
                        $social_media = get_field('linki_social_media');
                        if($social_media):
                    ?>
                    <div class="constact-social">
                        <p>
                            <?php
                                $i = 0;
                                foreach($social_media as $s):
                                $link = $s['link'];
                            ?>
                            <?php if($i) echo '<span>|</span>'; ?>
                            <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                            <?php $i++; ?>
                            <?php endforeach; ?>
                        </p>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; else : ?>
            <?php get_template_part( 'parts/content', 'missing' ); ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
