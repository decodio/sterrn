<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>
    <?php if(is_front_page()): ?>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/styles/home.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/home.css"></noscript
    <?php endif; ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
