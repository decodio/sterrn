<?php
/*
Template Name: Specjalizacje
*/

get_header(); ?>

<main class="main services">
    <div class="wrapper">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="content">
                <div class="page-header">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="services-content">
                    <?php the_content(); ?>
                    <?php
                    $subpages = get_field('subpages');
                    if($subpages): ?>
                    <ul>
                        <?php foreach($subpages as $s): ?>
                        <?php $link = $s['link']; ?>
                        <li><h2><a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a></h2></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; else : ?>
            <?php get_template_part( 'parts/content', 'missing' ); ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
