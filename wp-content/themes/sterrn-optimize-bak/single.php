<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<main class="main blog">
    <div class="wrapper">
        <div class="content">
            <div class="page-header">
                <?php
                    echo single_post_breadcrumb();
                ?>
            </div>
            <div class="blog-single">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
    		    <?php endwhile; else : ?>
    		   		<?php get_template_part( 'parts/content', 'missing' ); ?>
    		    <?php endif; ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
