<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>

<main class="main blog">
    <div class="wrapper">
        <div class="content">
            <div class="page-header">
                <p class="blog-single-breadcrumbs">
                    <span><a href="/publikacje/">Publikacje</a></span>
                    <span><?php echo single_cat_title( '', false );?></span>
                </p>
            </div>
            <div class="blog-list">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <!-- To see additional archive styles, visit the /parts directory -->
                    <?php get_template_part( 'parts/loop', 'archive' ); ?>
                    <?php endwhile; ?>
                    <div class="blog-prev-next">
                        <span class="next"><?php echo next_posts_link('Starsze wpisy'); ?></span>
                        <span class="prev"><?php echo previous_posts_link('Nowsze wpisy'); ?></span>
                    </div>
                <?php else : ?>
                    <?php get_template_part( 'parts/content', 'missing' ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
