<?php
/**
* For more info: https://developer.wordpress.org/themes/basics/theme-functions/
*
*/

// Theme support options
require_once(get_template_directory().'/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/functions/translation/translation.php');

// Adds site styles to the WordPress editor
// require_once(get_template_directory().'/functions/editor-styles.php');

// Remove Emoji Support
// require_once(get_template_directory().'/functions/disable-emoji.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/functions/related-posts.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/functions/admin.php');

function single_post_breadcrumb_category_id($post_id) {
    if ( class_exists('WPSEO_Primary_Term') ) {

        // Show the post's 'Primary' category, if this Yoast feature is available, & one is set. category can be replaced with custom terms

        $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $post_id );

        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $term               = get_term( $wpseo_primary_term );

        if (is_wp_error($term)) {
            $categories = get_the_terms($post_id, 'category');
            $cat_id = $categories[0]->term_id;
        } else {
            $cat_id = $term->term_id;
        }

    } else {
        $categories = get_the_terms($post_id, 'category');
        $cat_id = $categories[0]->term_id;
    }

    return $cat_id;
}

function single_post_breadcrumb() {
    global $post;
    $category = get_term(single_post_breadcrumb_category_id($post->ID), 'category');
    $category_link = get_term_link($category->term_id, 'category');
    $breadcrumbs = '<p class="blog-single-breadcrumbs">';
    $breadcrumbs .= '<span><a href="'.get_permalink( get_option( 'page_for_posts' ) ).'">Publikacje</a></span>';
    $breadcrumbs .= '<span><a href="'.$category_link.'">'.$category->name.'</a></span>';
    $breadcrumbs .= '<span>'.get_the_time( __('j.m.Y', 'jointswp') ).'</span>';
    $breadcrumbs .= '</p>';
    return $breadcrumbs;
}

function single_page_breadcrumb() {
    global $post;
    $breadcrumbs = '<p class="page-single-breadcrumbs">';
    $parent = $post->post_parent;
    if($parent) {
        $breadcrumbs .= '<span><a href="'.get_permalink( $parent ).'">'.get_the_title( $parent ).'</a></span>';
        $parent_class = 'has-parent';
    }
    $breadcrumbs .= '<span class="'.$parent_class.'">'.get_the_title().'</span>';
    $breadcrumbs .= '</p>';
    return $breadcrumbs;
}
