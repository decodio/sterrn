<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">

		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>
    


	</head>

	<body <?php body_class('nav-hidden'); ?>>

        <div class="navigation">
            <div class="logo">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sterrn_logo-01.svg" alt="Sterrn Logo">
                </a>
            </div>
            <div class="sidebar">
                <button class="sidebar-toggle">
                    <span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="sidebar-nav">
                    <?php joints_top_nav(); ?>
                </div>
                <div class="sidebar-decor">
                    <span><span></span></span>
                    <span><span></span></span>
                </div>
            </div>
            <div class="overflow"></div>
        </div>
