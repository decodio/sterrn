<?php
/*
Template Name: Zespół
*/

get_header(); ?>

<main class="main team">
    <div class="wrapper">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="content">
                <div class="page-header">
                    <h1><?php the_title(); ?></h1>
                </div>
                <?php if(get_field('tekst_naglowek')): ?>
                <div class="team-content">
                    <div class="team-content-top">
                        <p><?php the_field('tekst_naglowek'); ?></p>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="team-bottom">
                <div>
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="team-listing">
                <h2>Poznaj nas:</h2>
                <div class="grid-x grid-margin-x">
                    <?php
                    // the query
                    $args = array(
                        'post_type' => 'zespol',
                        'posts_per_page' => -1,
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                    );
                    $the_query = new WP_Query( $args ); ?>

                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="cell medium-4">
                                <div class="team-listing-item">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="team-listing-image">
                                            <?php the_post_thumbnail( 'full' ); ?>
                                        </div>
                                        <h3><?php the_title(); ?></h3>
                                        <?php if(get_field('stanowisko')): ?>
                                        <p><?php the_field('stanowisko'); ?></p>
                                        <?php endif; ?>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; else : ?>
            <?php get_template_part( 'parts/content', 'missing' ); ?>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>
