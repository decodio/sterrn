<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>

<main class="main home vh">
    <div class="wrapper">
        <div class="content">
            <?php if(get_field('tekst_hero')): ?>
            <h1><?php the_field('tekst_hero'); ?></h1>
            <?php endif; ?>
        </div>
        <div class="decor" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/sterrn_home.jpg)"></div>
    </div>
</main>

<?php get_footer(); ?>
