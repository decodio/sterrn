<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
*/

get_header(); ?>

<main class="main page">
    <div class="wrapper">
        <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="page-header">
                    <?php
                        echo single_page_breadcrumb();
                    ?>
                </div>
                <div class="page-content">
                    <?php get_template_part( 'parts/loop', 'page' ); ?>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>
