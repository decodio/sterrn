<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>

<main class="main blog">
    <div class="wrapper">
        <div class="content">
            <div class="page-header">
                <h1>Publikacje</h1>
            </div>
            <div class="blog-list">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <!-- To see additional archive styles, visit the /parts directory -->
                    <?php get_template_part( 'parts/loop', 'archive' ); ?>
                    <?php endwhile; ?>
                    <div class="blog-prev-next">
                        <span class="next"><?php echo next_posts_link('Starsze wpisy'); ?></span>
                        <span class="prev"><?php echo previous_posts_link('Nowsze wpisy'); ?></span>
                    </div>
                <?php else : ?>
                    <?php get_template_part( 'parts/content', 'missing' ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
